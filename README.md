#QuickJob
敏捷可配快速的任务调度平台

#更新日志
1. SpringPlugin 废弃掉，后面的版本会移除代码 使用 BaseProjectController 中 getClassBeanByName(String beanName) 替代，
 SpringPlugin 不是单例模式 对接dubbo服务会有问题。
2. 移植jfinalAdmin全部功能可作为后台管理系统使用。
3. 精简部分测试代码。

### QuickJob敏捷可配快速的任务调度平台
```
    Quartz提供两种基本作业存储类型。第一种类型叫做RAMJobStore，第二种类型叫做JDBC作业存储。
在默认情况下Quartz将任务调度的运行信息保存在内存中，这种方法提供了最佳的性能，
因为内存中数据访问最快。不足之处是缺乏数据的持久性，当程序路途停止或系统崩溃时，
所有运行的信息都会丢失。
    QuickJob基于JDBC作业存储方式，实现了对任务的灵活配置。
一个执行类加一个任务即可完成任务调度，化繁为简。执行类的方法 
**强烈推荐** 
返回值：string，入参
 **强烈推荐** 
基本数据类型，当然理论上支持无参方法以及无返回值方法。
```

### 部署推荐
 _maven部署 推荐使用 idea 作为开发工具 账号 admin 密码123456_


![输入图片说明](https://git.oschina.net/uploads/images/2017/0714/183153_f89356e2_22738.png "在这里输入图片标题")

![输入图片说明](https://git.oschina.net/uploads/images/2017/0714/183207_5ac108cf_22738.png "在这里输入图片标题")

![输入图片说明](https://git.oschina.net/uploads/images/2017/0714/183216_c61dae9c_22738.png "在这里输入图片标题")

![输入图片说明](https://git.oschina.net/uploads/images/2017/0714/183227_fd998584_22738.png "在这里输入图片标题")

![输入图片说明](https://git.oschina.net/uploads/images/2017/0714/183239_804cd654_22738.png "在这里输入图片标题")

![输入图片说明](https://git.oschina.net/uploads/images/2017/0714/183251_4195c101_22738.png "在这里输入图片标题")

### 执行类如何编写
```
public class TestJob extends BaseJob {

    public String test(String name, String emali) {
        System.out.println(">>>>: name->" + name + " emali->" + emali);
        return "";
    }
}
```








