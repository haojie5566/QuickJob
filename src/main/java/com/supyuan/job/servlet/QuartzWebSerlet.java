package com.supyuan.job.servlet;



import com.supyuan.job.JobFactoryServiceImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 提供Servlet 加载方式
 * Created by yuanxuyun on 2017/4/19.
 */
public class QuartzWebSerlet extends HttpServlet {



    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void init() throws ServletException {

        System.out.println("---------加载任务启用的job到任务池---------");
        JobFactoryServiceImpl factoryService = new JobFactoryServiceImpl();
        //加载job
        factoryService.addJobList();
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.service(req, resp);
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
